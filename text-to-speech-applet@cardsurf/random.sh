#!/bin/bash

# Automatically split array elements in for loops on newline only
IFS=$'\n'

# Declare input variables
text=$1

# Read text into array of lines
lines=($(echo "$text"))

# For each line
for line in "${lines[@]}"; do

    # Generate accent
    voices=($(espeak --voices))
    voices=("${voices[@]:1}") # Remove first array element
    high=$((${#voices[@]}-1))
    index=$(shuf -i 0-"$high" -n 1)
    accent=$(echo "${voices[$index]}" | gawk '{match($0, /[[:alpha:]-]+/, matches); print matches[0]}')

    # Generate gender
    index=$(shuf -i 0-1 -n 1)
    gender="m"
    if [ "$index" -eq "1" ]; then
        gender="f"
    fi

    # Generate type
    type=$(shuf -i 1-5 -n 1)

    # Generate amplitude
    amplitude=$(shuf -i 10-20 -n 1)

    # Generate pitch
    pitch=$(shuf -i 0-99 -n 1)

    # Generate speed
    speed=$(shuf -i 75-175 -n 1)

    # Read line
    espeak -v "$accent+$gender$type" -a "$amplitude" -p "$pitch" -s "$speed" "$line" 

done
