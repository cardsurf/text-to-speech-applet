# Changelog

### Version 1.3.1
Reading is unpaused on left mouse click

### Version 1.3.0
Removed espeak dependency

### Version 1.2.2
Replaced tempfile command with mktemp

### Version 1.2.1
Added support for Cinnamon 3.8

### Version 1.2.0
Added support for Cinnamon 3.4  
Added support for keyboard shortcuts  
Added Croatian translation (thanks [muzena](https://github.com/muzena))  
Added Swedish translation (thanks [eson57](https://github.com/eson57))  
Added Danish translation (thanks [Alan01](https://github.com/Alan01))  

### Version 1.1.0
Added support for vertical panels  
Added translation support (thanks [giwhub](https://github.com/giwhub))  
Added Chinese translation (thanks [giwhub](https://github.com/giwhub))  
Fixed an issue that prevented to set custom icons in Cinnamon 3.2

### Version 1.0.0
Initial release
